import { PrivacyPolicyComponent } from './pages/common/privacy-policy/privacy-policy.component';
import { TermsOfUseComponent } from './pages/common/terms-of-use/terms-of-use.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ResetEmailPasswordComponent } from './pages/shared/component/reset-email-password/reset-email-password.component';
import { VerifyEmailLinkComponent } from './pages/shared/component/verify-email-link/verify-email-link.component';
import { ROUTE_PATHS } from './pages/global/constants';


const routes: Routes = [
  { path: '', redirectTo: ROUTE_PATHS.HOME, pathMatch: 'full' },
  { path: ROUTE_PATHS.HOME, component: HomeComponent },
  { path: ROUTE_PATHS.RESET_PASSWORD, component: ResetEmailPasswordComponent },
  { path: ROUTE_PATHS.VERIFY_PASSWORD, component: VerifyEmailLinkComponent },
  { path: ROUTE_PATHS.PRIVACY_POLICY, component: PrivacyPolicyComponent },
  { path: ROUTE_PATHS.TERMS_AND_CONDITION, component: TermsOfUseComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
