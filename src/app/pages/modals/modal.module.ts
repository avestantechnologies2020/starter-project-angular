import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { LoginComponent } from './login/login.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SharedModule } from '../shared/shared.module';
import { CountdownModule, CountdownConfig, CountdownGlobalConfig } from 'ngx-countdown';
import { NgOtpInputModule } from 'ng-otp-input';

export function countdownConfigFactory(): CountdownConfig {
  return { format: `mm:ss` };
}
@NgModule({
  declarations: [
    LoginComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    ModalModule,
    SharedModule,
    CountdownModule,
    NgOtpInputModule
  ],
  exports: [
    LoginComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent
  ],
  providers: [{ provide: CountdownGlobalConfig, useFactory: countdownConfigFactory }],
})
export class ModalsModule { }
