import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { LoginComponent } from '../login/login.component';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { FormService } from '../../shared/services/form.service';
import { AuthenticationService } from '../../shared/services/auth/auth.service';
import { LocalStorageService } from '../../shared/services/local-storage.service';
import { RegexEnum } from '../../global/regex-enum';
import { constants, LOCAL_STORAGE_KEYS } from '../../global/constants';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  private response: Subject<any>;
  forgotForm: FormGroup;
  messageList: any = {};
  showEmail;
  email = constants.EMAIL;
  mobileNumber = constants.MOBILE_NUMBER;
  type;

  constructor(
    private modalService: BsModalService,
    private modalRef: BsModalRef,
    private formBuilder: FormBuilder,
    public translation: TranslateService,
    private formService: FormService,
    private authService: AuthenticationService,
    private localStorage: LocalStorageService,
    private ngxLoader: NgxUiLoaderService,
  ) { }

  ngOnInit() {
    this.response = new Subject();
    this.intializingForgotForm();
    this.intializingMessage();
  }

  intializingForgotForm() {
    return this.forgotForm = this.formBuilder.group({
      mobile: ['', [Validators.required, , Validators.pattern(RegexEnum.mobile)]],
      type: [this.mobileNumber, Validators.required]
    });
  }

  intializingMessage() {
    this.messageList.mobile = {
      pattern: this.translation.instant('ERR_MSG_INVALID_MOBILE_NUMBER'),
      required: this.translation.instant('ERR_MSG_MOBILE_REQUIRED'),
    };

    this.messageList.email = {
      pattern: this.translation.instant('ERR_MSG_INVALID_EMAIL_ADDRESS'),
      required: this.translation.instant('ERR_MSG_EMAIL_REQUIRED'),
    };
  }

  onHide() {
    this.modalRef.hide();
  }

  onLogin() {
    this.onHide();
    const initialState = {};
    this.modalService.show(LoginComponent, { initialState, class: 'modal-lg' });
  }

  typeSelected() {
    if (this.forgotForm.value.type === this.email) {
      this.showEmail = true;
      this.forgotForm.addControl('email', new FormControl('', [Validators.required, Validators.pattern(RegexEnum.email)]));
      this.forgotForm.get('mobile').setValue(null);
      this.forgotForm.get('mobile').setErrors(null);
    } else {
      this.showEmail = false;
      this.forgotForm.get('email').setValue(null);
      this.forgotForm.get('email').setErrors(null);
      this.forgotForm.get('mobile').setValidators([Validators.required, , Validators.pattern(RegexEnum.mobile)]);
    }
  }
  async onForgotPassword() {
    this.formService.markFormGroupTouched(this.forgotForm);
    if (this.forgotForm.valid) {
      this.ngxLoader.start();
      const data: any = this.forgotForm.getRawValue();
      if (data.mobile) {
        const response: any = await this.authService.forgotPassword({ mobile: data.mobile });
        if (response && response.data && response.data.userId) {
          await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.ID, response.data.userId);
          this.onHide();
          const initialState = {
            type: constants.OTP_FROM.FORGOT_PASSWORD_MOBILE,
            mobile: data.mobile
          };
        }
      } else {
        this.onHide();
        const response: any = await this.authService.forgotPassword({ email: data.email });
      }

    }
  }
}
