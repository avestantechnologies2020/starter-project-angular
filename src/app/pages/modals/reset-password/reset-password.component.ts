import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { LoginComponent } from '../login/login.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormService } from '../../shared/services/form.service';
import { TranslateService } from '@ngx-translate/core';
import { RegexEnum } from '../../global/regex-enum';
import { UtilityService } from '../../shared/services/utility.service';
import { LocalStorageService } from '../../shared/services/local-storage.service';
import { LOCAL_STORAGE_KEYS } from '../../global/constants';
import { AuthenticationService } from '../../shared/services/auth/auth.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  public resetPasswordForm: FormGroup;
  private response: Subject<any>;
  public messageList: any = {};
  private userId;
  constructor(
    private modalService: BsModalService,
    private modalRef: BsModalRef,
    private fb: FormBuilder,
    private formService: FormService,
    public translation: TranslateService,
    private localStorage: LocalStorageService,
    private authService: AuthenticationService,
    private ngxLoader: NgxUiLoaderService,
  ) { }

  async ngOnInit() {
    this.userId = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE_KEYS.ID);
    this.response = new Subject();
    this.initializeResetPasswordForm();
    this.initializeMessages();
  }

  onHide() {
    this.modalRef.hide();
  }

  initializeResetPasswordForm() {
    this.resetPasswordForm = this.fb.group({
      password: ['', [
        Validators.required,
        Validators.pattern(RegexEnum.passwordValidation)]
      ],
      confirmpassword: ['', [
        Validators.required]
      ],
    },
      { validator: UtilityService.MatchPassword }
    );
  }

  initializeMessages() {
    this.messageList.password = {
      pattern: this.translation.instant('ERR_PASSWORD_INVALID'),
      required: this.translation.instant('ERR_MSG_PASSWORD_REQUIRED'),
    };
    this.messageList.confirmpassword = {
      required: this.translation.instant('ERR_PASSWORD_REQUIRED'),
    };
  }

  get resetPasswordFormControls() { return this.resetPasswordForm.controls; }

  onLogin() {
    this.onHide();
    const initialState = {};
    this.modalService.show(LoginComponent, { initialState, class: 'modal-lg' });
  }

  async onResetPassword() {
    this.formService.markFormGroupTouched(this.resetPasswordForm);
    if (this.resetPasswordForm.valid) {
      this.ngxLoader.start();
      const obj = {
        userId: this.userId,
        password: this.resetPasswordForm.get('password').value,
      };
      await this.authService.resetPassword(obj);
      this.onLogin();
    }
  }
}
