import { ProfileService } from './../../shared/services/data-communication-services/profile.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SignUpComponent } from '../sign-up/sign-up.component';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegexEnum } from '../../global/regex-enum';
import { TranslateService } from '@ngx-translate/core';
import { FormService } from '../../shared/services/form.service';
import { AuthenticationService } from '../../shared/services/auth/auth.service';
import { LocalStorageService } from '../../shared/services/local-storage.service';
import { LOCAL_STORAGE_KEYS, constants, KYC_STATUS } from '../../global/constants';
import { HttpErrorHandler } from '../../shared/services/error-handler/http-error-handler.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  private response: Subject<any>;
  loginForm: FormGroup;
  subscription: Subscription;
  messageList: any = {};
  agent = constants.ROLE.AGENT;

  constructor(
    private modalService: BsModalService,
    private modalRef: BsModalRef,
    private formBuilder: FormBuilder,
    public translation: TranslateService,
    private formService: FormService,
    private authService: AuthenticationService,
    private localStorage: LocalStorageService,
    private profileService: ProfileService,
    private errorHandler: HttpErrorHandler,
    private ngxLoader: NgxUiLoaderService,
  ) { }

  ngOnInit() {
    this.response = new Subject();
    this.intializingUserForm();
    this.intializingMessage();
    this.getMobileNotVerfied();
    this.getEmailNotVerfied();
  }

  intializingUserForm() {
    return this.loginForm = this.formBuilder.group({
      mobile: ['', [Validators.required, , Validators.pattern(RegexEnum.mobile)]],
      password: ['', Validators.required],
    });
  }

  intializingMessage() {
    this.messageList.mobile = {
      pattern: this.translation.instant('ERR_MSG_INVALID_MOBILE_NUMBER'),
      required: this.translation.instant('ERR_MSG_MOBILE_REQUIRED'),
    };

    this.messageList.password = {
      required: this.translation.instant('ERR_MSG_PASSWORD_REQUIRED'),
    };
  }

  onHide() {
    this.modalRef.hide();
  }

  onSignup() {
    this.onHide();
    const initialState = {};
    this.modalService.show(SignUpComponent, { initialState, class: 'modal-lg' });
  }

  onForgotPassword() {
    this.onHide();
    const initialState = {};
    this.modalService.show(ForgotPasswordComponent, { initialState, class: 'modal-lg' });
  }

  async onLogin() {
    this.formService.markFormGroupTouched(this.loginForm);
    if (this.loginForm.valid) {
      this.ngxLoader.start();
      const response: any = await this.authService.login(this.loginForm.getRawValue());
      if (response && response.token) {
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.TOKEN, response.token);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.ID, response.user.id);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.ROLE, response.user.role);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.IS_TRANSACTION_PIN, response.user.isTransactionPin);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.BUSINESS_CATEGORIES, response.user.businessCategories);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.FIRSTNAME, response.user.firstName);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.LASTNAME, response.user.lastName);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.EMAIL, response.user.email);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.IS_TRANSACTION_PIN, response.user.isTransactionPin);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.QR_CODE, response.user.qrCode);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.PROFILE, response.user.profilePicture);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.BUSINESS_NAME, response.user.businessName);
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.MOBILE, response.user.mobile);
        this.onHide();
        const initialState = {};
        if (response.user && response.user.role === this.agent && response.user.isDocumentUploaded === false) {
        } else if (response.user && !response.user.bvnNumber && response.user.kycStatus === constants.KYC_STATUS.NOT_VERIFIED) {
        }
        const isVerified = response.user && response.user.kycStatus === constants.KYC_STATUS.VERIFIED;
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.KYC_STATUS, isVerified);
        this.profileService.setKycStatus(response.user.kycStatus ===  KYC_STATUS.VERIFIED ? true : false);
      }
    }
  }

  async getMobileNotVerfied() {
    this.subscription = this.errorHandler.mobileNotverified.asObservable().subscribe(async (notVerified: any) => {
      if (notVerified) {
        this.onHide();
        const initialState = {
          mobile: this.loginForm.value.mobile,
          title: this.translation.instant('MOBILE_NUMBER_NOT_VALID_TITLE'),
          text: this.translation.instant('MOBILE_NUMBER_NOT_VALID_TEXT'),
        };
        this.modalRef.content.response.subscribe(async (shouldYes: any) => {
          if (!shouldYes) { return; }
          this.errorHandler.mobileNotverified.next(false);
        });
      }
    });
  }

  async getEmailNotVerfied() {
    this.subscription = this.errorHandler.emailNotverified.asObservable().subscribe(async (notVerified: any) => {
      if (notVerified) {
        this.onHide();
        const initialState = {
          mobile: false,
          title: this.translation.instant('EMAIL_NOT_VALID_TITLE'),
          text: this.translation.instant('EMAIL_NOT_VALID_TEXT'),
        };
        this.errorHandler.emailNotverified.next(false);
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
