import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LoginComponent } from '../login/login.component';
import { AuthenticationService } from '../../shared/services/auth/auth.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { FormService } from '../../shared/services/form.service';
import { UtilityService } from '../../shared/services/utility.service';
import { RegexEnum } from '../../global/regex-enum';
import { constants, LOCAL_STORAGE_KEYS } from '../../global/constants';
import { LocalStorageService } from '../../shared/services/local-storage.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  private response: Subject<any>;
  userSignUpForm: FormGroup;
  agentSignUpForm: FormGroup;
  messageList: any = {};
  agent = constants.ROLE.AGENT;
  user = constants.ROLE.USER;
  agentFormEnabled;
  categoriesData = constants.BUSINESS_CATEGORIES;

  constructor(
    private modalRef: BsModalRef,
    private modalService: BsModalService,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    public translation: TranslateService,
    private formService: FormService,
    private localStorage: LocalStorageService,
    private utilsService: UtilityService,
    private ngxLoader: NgxUiLoaderService,
  ) { }

  ngOnInit() {
    this.response = new Subject();
    this.intializingUserForm();
    this.intializingMessage();
  }

  intializingUserForm() {
    return this.userSignUpForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern(RegexEnum.name)]],
      lastName: ['', [Validators.required, Validators.pattern(RegexEnum.name)]],
      mobile: ['', [Validators.required, , Validators.pattern(RegexEnum.mobile)]],
      role: [this.user, Validators.required],
      email: ['', [Validators.required, Validators.pattern(RegexEnum.email)]],
      password: ['', [Validators.required, Validators.pattern(RegexEnum.passwordValidation)]],
    });
  }

  intializingMessage() {
    this.messageList.firstName = {
      pattern: this.translation.instant('ERR_MSG_ONLY_CHARACTER_ALLOWED'),
      required: this.translation.instant('ERR_MSG_FIRSTNAME_REQUIRED'),
    };

    this.messageList.lastName = {
      pattern: this.translation.instant('ERR_MSG_ONLY_CHARACTER_ALLOWED'),
      required: this.translation.instant('ERR_MSG_LAST_REQUIRED'),
    };

    this.messageList.businessName = {
      pattern: this.translation.instant('ERR_MSG_ONLY_CHARACTER_ALLOWED'),
      required: this.translation.instant('ERR_MSG_BUSINESSNAME_REQUIRED'),
    };

    this.messageList.businessCategories = {
      required: this.translation.instant('ERR_MSG_BUSINESS_CATEGORIES_REQUIRED'),
    };

    this.messageList.businessDescription = {
      required: this.translation.instant('ERR_MSG_BUSINESS_DESC_REQUIRED'),
    };

    this.messageList.email = {
      pattern: this.translation.instant('ERR_MSG_INVALID_EMAIL_ADDRESS'),
      required: this.translation.instant('ERR_MSG_EMAIL_REQUIRED'),
    };
    this.messageList.mobile = {
      pattern: this.translation.instant('ERR_MSG_INVALID_MOBILE_NUMBER'),
      required: this.translation.instant('ERR_MSG_MOBILE_REQUIRED'),
    };

    this.messageList.password = {
      pattern: this.translation.instant('ERR_PASSWORD_INVALID'),
      required: this.translation.instant('ERR_MSG_PASSWORD_REQUIRED'),
    };
  }

  onHide() {
    this.modalRef.hide();
  }

  onLogin() {
    this.onHide();
    const initialState = {};
    this.modalService.show(LoginComponent, { initialState, class: 'modal-lg' });
  }

  roleSelected() {
    if (this.userSignUpForm.value.role === this.agent) {
      this.agentFormEnabled = true;
      this.userSignUpForm.addControl('businessName', new FormControl('', [Validators.required, Validators.pattern(RegexEnum.name)]));
      this.userSignUpForm.addControl('businessCategories', new FormControl('', [Validators.required]));
      this.userSignUpForm.addControl('businessDescription', new FormControl('', Validators.required));
      this.userSignUpForm.get('firstName').setErrors(null);
      this.userSignUpForm.get('lastName').setErrors(null);
    } else {
      this.agentFormEnabled = false;
      this.userSignUpForm.get('businessName').setErrors(null);
      this.userSignUpForm.get('businessCategories').setErrors(null);
      this.userSignUpForm.get('businessDescription').setErrors(null);
      this.userSignUpForm.get('firstName').setValidators([Validators.required, Validators.pattern(RegexEnum.name)]);
      this.userSignUpForm.get('lastName').setValidators([Validators.required, Validators.pattern(RegexEnum.name)]);
    }
  }

  async onSignUp() {
    this.formService.markFormGroupTouched(this.userSignUpForm);
    if (this.userSignUpForm.valid) {
      this.ngxLoader.start();
      const sendData = await this.utilsService.cleanObject(this.userSignUpForm.getRawValue());
      const response: any = await this.authService.signUp(sendData);
      if (response.message) {
        await this.localStorage.setDataInIndexedDB(LOCAL_STORAGE_KEYS.MOBILE, sendData.mobile);
        this.onHide();
        const initialState = {};
      }
    }
  }
}
