import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WalletSetupComponent } from './wallet-setup/wallet-setup.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    WalletSetupComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class HomeModule { }
